# ajax_pagination.js

## Setup

- Include `dist/ajax_pagination.js` script in the page.
- Add the `data-dwaiter-ajax-pagination` attribute to a Collection List Wrapper with any unique value. The value is used to distinguish between Collection List Wrappers if there are multiple on a page.
- Set the text color of the next and previous links to the color you want the loading spinner to be
- Initialize the script:

```javascript
dwaiterAjaxPagination()
```


# collection_filter_sort.js

## Setup

- Include `dist/collection_filter_sort.js` script in the page.
- Put an empty `<select>` element on the page for every filter, one for sort type, and one for sort direction
- Use code like the following:

```javascript
dwaiterCollectionFilter({
    itemParent: document.querySelector('.list'),
    filters: [
        {
            id: 'category-1',
            selectElement: document.querySelector('.select-1'),
            dataSelector: '.category',
            dataSplitOnCommas: false,
        },
        {
            id: 'category-2',
            selectElement: document.querySelector('.select-2'),
            dataSelector: '.category',
            dataSplitOnCommas: true,
        },
    ],
});

dwaiterCollectionSort({
    itemParent: document.querySelector('.list'),
    selectElement: document.querySelector('.select-3'),
    directionElement: document.querySelector('.select-4'),
    fields: [
        {
            id: 'category-1',
            title: 'One',
            dataSelector: '.sort-one',
            type: 'string',
        },
        {
            id: 'category-2',
            title: 'Two',
            dataSelector: '.sort-two',
            type: 'number',
        },
    ],
});
```

## Configuration Options

- `dwaiterCollectionFilter`
    - `itemParent`: the direct parent element of the elements you are sorting or filtering.
    - `filters`: use one for each field you want to filter by.
        - `id`: the URL parameter name for that filter.
        - `selectElement`: the DOM element you created for that `<select>`.
        - `dataSelector`: a CSS selector that refers to the hidden DOM element under each item where the field value is stored.
        - `dataSplitOnCommas`: whether to treat the content of `dataSelector` as multiple items separated by commas or as a single item. Set this to `false` if you don't need the comma splitting especially if your field may contain commas.

- `dwaiterCollectionSort`
    - `itemParent`: the direct parent element of the elements you are sorting or filtering.
    - `selectElement`: the DOM element you created for that `<select>`.
    - `directionElement`: the DOM element you created for the `<select>` that will choose between sorting in ascending versus descending order.
    - `fields`: use one for each field you want to sort by.
        - `id`: the URL parameter name for that filter.
        - `title`: the name that shows in the sort dropdown.
        - `dataSelector`: a CSS selector that refers to the hidden DOM element under each item where the field value is stored.
        - `type`: either `'string'` or `'number'` to specify whether to treat the value in `dataSelector` as a string or a number. This will affect the sorting comparison. For example the string `'10'` is less than `'2'` alphabetically. But the number `10` is greater than `2`.


# zip_filter.js

## Setup

- Include `dist/zip_filter.js` script in the page.
- Put an `<input>` element on the page inside a form to capture zip codes.
- Use code like the following:

```javascript
dwaiterZipFilter({
    apiKey: 'YOUR_API_KEY',
    zipInputElement: document.querySelector('.input'),
    itemParent: document.querySelector('.items'),
    latitudeSelector: '.lat',
    longitudeSelector: '.lng',
    distanceSelector: '.distance',
    numberOfResults: 10,
});
```

## Configuration Options

- `apiKey`: the API key for Google Maps. This must include access to the "Maps JavaScript API" and the "Geocoding API". Since the API is billed by usage, you should have the client sign up for this account.
- `zipInputElement`: the DOM element you created for the zip `<input>`.
- `itemParent`: the direct parent element of the elements you are sorting or filtering.
- `latitudeSelector`: a CSS selector that refers to the hidden DOM element under each item where the latitude is stored.
- `longitudeSelector`: a CSS selector that refers to the hidden DOM element under each item where the longitude is stored.
- `distanceSelector`: a CSS selector that refers to the DOM element under each item where the distance will be injected.
- `numberOfResults`: the number of results to show sorted by distance from the entered zip.
