(function() {
    type Config = {
        apiKey: string;
        zipInputElement: HTMLInputElement;
        itemParent: HTMLElement;
        latitudeSelector: string;
        longitudeSelector: string;
        distanceSelector: string;
        numberOfResults: number;
    }

    type FilterItem = {
        item: HTMLElement;
        location: Location;
        distance: number;
    };

    type Location = {
        lat: number;
        lng: number;
    }

    function init(config: Config): void {
        validateConfig(config);
        loadScript('https://maps.googleapis.com/maps/api/js?libraries=geometry&key=' + config.apiKey).then(() => {
            initAfterMaps(config);
        });
    }

    function validateConfig(config: Config): void {
        if (!(typeof config.apiKey === 'string')) {
            console.error('dwaiterZipFilter invalid configuration option: apiKey is not a string');
        }

        if (!(config.zipInputElement instanceof HTMLInputElement)) {
            console.error('dwaiterZipFilter invalid configuration option: zipInputElement is not an HTMLInputElement');
        }

        if (!(config.itemParent instanceof HTMLElement)) {
            console.error('dwaiterZipFilter invalid configuration option: itemParent is not an HTMLElement');
        }

        if (!(typeof config.numberOfResults === 'number')) {
            console.error('dwaiterZipFilter invalid configuration option: numberOfResults is not a number');
        }

        if (config.zipInputElement.closest('form') === null) {
            console.error('dwaiterZipFilter invalid configuration option: zipInputElement must be within a form');
        }

        for (const item of Array.from(config.itemParent.children)) {
            const latitudeElements = item.querySelectorAll(config.latitudeSelector);
            if (latitudeElements.length !== 1) {
                console.error('dwaiterZipFilter invalid configuration option: latitudeSelector matches ' + latitudeElements.length + ' elements but should match exactly 1 in item', item);
            } else if (Number.isNaN(parseFloat(assertNotNull(latitudeElements[0].textContent)))) {
                console.error('dwaiterZipFilter invalid configuration option: latitudeSelector is not a number in item', item);
            }

            const longitudeElements = item.querySelectorAll(config.longitudeSelector);
            if (longitudeElements.length !== 1) {
                console.error('dwaiterZipFilter invalid configuration option: longitudeSelector matches ' + longitudeElements.length + ' elements but should match exactly 1 in item', item);
            } else if (Number.isNaN(parseFloat(assertNotNull(longitudeElements[0].textContent)))) {
                console.error('dwaiterZipFilter invalid configuration option: longitudeSelector is not a number in item', item);
            }

            const distanceElements = item.querySelectorAll(config.distanceSelector);
            if (distanceElements.length !== 1) {
                console.error('dwaiterZipFilter invalid configuration option: distanceSelector matches ' + distanceElements.length + ' elements but should match exactly 1 in item', item);
            }
        }
    }

    function initAfterMaps(config: Config): void {
        const form = assertNotNull(config.zipInputElement.closest('form'));
        const items = Array.from(config.itemParent.children) as HTMLElement[];
        const filterItems = getFilterItems(items, config.latitudeSelector, config.longitudeSelector);

        hideFilterItems(filterItems);

        config.zipInputElement.addEventListener('input', (e) => {
            config.zipInputElement.value = cleanZip(config.zipInputElement.value);
        });

        form.addEventListener('submit', (e) => {
            e.preventDefault();

            const zip = config.zipInputElement.value;

            if (!isValidZip(zip)) return;

            hideFilterItems(filterItems);

            getZipLocation(zip).then((location) => {
                filter(filterItems, location, config);
            }).catch((error) => {
                console.error('dwaiterZipFilter ' + error);
            });
        });
    }

    function loadScript(url: string): Promise<void> {
        const script = document.createElement('script');
        script.async = true;
        script.defer = true;

        return new Promise((resolve, reject) => {
            script.addEventListener('load', () => resolve());
            script.src = url;
            document.head.appendChild(script);
        });
    }

    function filter(filterItems: FilterItem[], location: Location, config: Config): void {
        const distanceFilterItems = setFilterItemsDistance(filterItems, location);
        const sortedFilterItems = distanceFilterItems.sort(compareFilterItemDistance);

        sortedFilterItems.forEach((filterItem, i) => {
            if (i < config.numberOfResults) {
                const distanceElement = assertNotNull(filterItem.item.querySelector(config.distanceSelector));
                distanceElement.textContent = Math.round(filterItem.distance).toLocaleString();
                filterItem.item.style.removeProperty('display');
            } else {
                filterItem.item.style.display = 'none';
            }

            config.itemParent.appendChild(filterItem.item);
        });
    }

    function cleanZip(zip: string): string {
        return zip.replace(/\D/g, '').substring(0, 5);
    }

    function isValidZip(zip: string): boolean {
        return zip.match(/\d\d\d\d\d/) !== null;
    }

    function hideFilterItems(filterItems: FilterItem[]): void {
        for (const filterItem of filterItems) {
            filterItem.item.style.display = 'none';
        }
    }

    function compareFilterItemDistance(a: FilterItem, b: FilterItem): number {
        if (a.distance < b.distance) return -1;
        else if (a.distance > b.distance) return 1;
        else return 0;
    }

    function setFilterItemsDistance(filterItems: FilterItem[], location: Location): FilterItem[] {
        const google = (window as any).google;

        return filterItems.map((filterItem) => {
            const distanceMeters = google.maps.geometry.spherical.computeDistanceBetween(
                new google.maps.LatLng(location),
                new google.maps.LatLng(filterItem.location),
            );

            const distance = metersToMiles(distanceMeters);

            return {
                ...filterItem,
                distance: distance,
            };
        });
    }

    function getFilterItems(items: HTMLElement[], latSelector: string, lngSelector: string): FilterItem[] {
        const filterItems = [];

        for (const item of items) {
            filterItems.push({
                item: item,
                location: {
                    lat: parseFloat(assertNotNull(assertNotNull(item.querySelector(latSelector)).textContent)),
                    lng: parseFloat(assertNotNull(assertNotNull(item.querySelector(lngSelector)).textContent)),
                },
                distance: 0,
            });
        }

        return filterItems;
    }

    function getZipLocation(zip: string): Promise<Location> {
        const google = (window as any).google;
        const geocoder = new google.maps.Geocoder();
        const query = {
            address: zip,
            componentRestrictions: {
                country: 'US',
            },
        };

        return new Promise((resolve, reject) => {
            geocoder.geocode(query, (results: any, status: any) => {
                if (status !== 'OK') {
                    reject('geocode error: ' + status);
                    return;
                }

                if (results.length === 0) {
                    reject('geocode no results');
                    return;
                }

                const location = results[0].geometry.location;

                resolve({
                    lat: location.lat(),
                    lng: location.lng(),
                });
            });
        });
    }

    function metersToMiles(meters: number): number {
        return meters / 1609.344;
    }

    function assertNotNull<T>(obj: T | null): T {
        if (obj === null) {
            throw new Error('Non-null assertion failed');
        } else {
            return obj;
        }
    }

    (window as any).dwaiterZipFilter = init;
}());
