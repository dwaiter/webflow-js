type FilterConfig = {
    id: string;
    selectElement: HTMLSelectElement;
    dataSelector: string;
    dataSplitOnCommas: boolean;
};

type FilterItem = {
    item: HTMLElement;
    attributes: { [index: string]: string[] };
};

type FilterValue = {
    id: string;
    value: string;
};

type SortConfig = {
    id: string;
    title: string;
    dataSelector: string;
    type: 'string' | 'number';
};

type SortItem = {
    item: HTMLElement;
    attributes: { [index: string]: (string | number) };
};

type SortValue = {
    id: string;
    ascending: boolean;
};

(function() {
    function filterGetFilterItems(items: HTMLElement[], filterConfigs: FilterConfig[]): FilterItem[] {
        const filterItems = [];

        for (const item of items) {
            const filterItem: FilterItem = {
                item: item,
                attributes: {},
            };

            for (const filterConfig of filterConfigs) {
                const dataElement = item.querySelector(filterConfig.dataSelector);

                if (dataElement === null || dataElement.textContent === null) continue;

                let attributeValues = [];
                if (filterConfig.dataSplitOnCommas) {
                    attributeValues = splitCsv(dataElement.textContent);
                } else {
                    attributeValues = [dataElement.textContent];
                }
                filterItem.attributes[filterConfig.id] = attributeValues.filter((attributeValue) => attributeValue !== '');

                filterItems.push(filterItem);
            }
        }

        return filterItems;
    }

    function filterGetChoices(filterId: string, filterItems: FilterItem[]): string[] {
        let choices: string[] = [];

        for (const filterItem of filterItems) {
            const attributeValues = assertNotUndefined(filterItem.attributes[filterId]);
            choices = choices.concat(attributeValues);
        }

        return unique(choices).sort();
    }

    function filterPopulateSelectOptions(filterConfig: FilterConfig, filterItems: FilterItem[]): void {
        const choices = filterGetChoices(filterConfig.id, filterItems);

        const allOption = document.createElement('option');
        allOption.setAttribute('value', '');
        allOption.textContent = 'All';
        filterConfig.selectElement.appendChild(allOption);

        for (const choice of choices) {
            const option = document.createElement('option');
            option.textContent = choice;
            filterConfig.selectElement.appendChild(option);
        }
    }

    function filter(filterItems: FilterItem[], filterValues: FilterValue[]): void {
        for (const filterItem of filterItems) {
            const isMatch = filterValues.every((filterValue) => {
                const attributeValues = assertNotUndefined(filterItem.attributes[filterValue.id]);
                return attributeValues.indexOf(filterValue.value) >= 0;
            });

            if (isMatch) {
                filterItem.item.style.removeProperty('display');
            } else {
                filterItem.item.style.display = 'none';
            }
        }
    }

    function filterValuesFromQuery(filterConfigs: FilterConfig[], queryParams: { [index: string]: string }): FilterValue[] {
        const filterValues = [];

        for (const filterConfig of filterConfigs) {
            const value = queryParams[filterConfig.id];

            if (value !== undefined && value !== '') {
                filterValues.push({
                    id: filterConfig.id,
                    value: value,
                });
            }
        }

        return filterValues;
    }

    function filterValuesFromSelects(filterConfigs: FilterConfig[]): FilterValue[] {
        const filterValues = [];

        for (const filterConfig of filterConfigs) {
            const value = filterConfig.selectElement.value;

            if (value !== '') {
                filterValues.push({
                    id: filterConfig.id,
                    value: value,
                });
            }
        }

        return filterValues;
    }

    function filterValidateConfig(config: { itemParent: HTMLElement; filters: FilterConfig[] }): void {
        if (!(config.itemParent instanceof HTMLElement)) {
            console.error('dwaiterCollectionFilter invalid configuration option: itemParent is not an HTMLElement');
        }

        if (!(config.filters instanceof Array) || config.filters.length === 0) {
            console.error('dwaiterCollectionFilter invalid configuration option: no filters provided');
        }

        for (const filterConfig of config.filters) {
            if (typeof filterConfig.id !== 'string') {
                console.error('dwaiterCollectionFilter invalid configuration option: filter id is not a string');
            }

            if (!(filterConfig.selectElement instanceof HTMLSelectElement)) {
                console.error('dwaiterCollectionFilter invalid configuration option: filter (' + filterConfig.id + ') selectElement is not an HTMLSelectElement');
            }

            if (typeof filterConfig.dataSplitOnCommas !== 'boolean') {
                console.error('dwaiterCollectionFilter invalid configuration option: filter (' + filterConfig.id + ') dataSplitOnCommas is not a boolean');
            }
        }

        for (const item of Array.from(config.itemParent.children)) {
            for (const filterConfig of config.filters) {
                const elements = item.querySelectorAll(filterConfig.dataSelector);
                if (elements.length !== 1) {
                    console.error('dwaiterCollectionFilter invalid configuration option: filter (' + filterConfig.id + ') dataSelector matches ' + elements.length + ' elements but should match exactly 1');
                }
            }
        }

        const ids = config.filters.map(filterConfig => filterConfig.id);
        if (ids.length !== unique(ids).length) {
            console.error('dwaiterCollectionFilter invalid configuration option: duplicate filter id');
        }
    }

    function sortGetSortItems(items: HTMLElement[], sortConfigs: SortConfig[]): SortItem[] {
        const sortItems = [];

        for (const item of items) {
            const sortItem: SortItem = {
                item: item,
                attributes: {},
            };

            for (const sortConfig of sortConfigs) {
                const dataElement = item.querySelector(sortConfig.dataSelector);

                if (dataElement === null || dataElement.textContent === null) continue;

                if (sortConfig.type === 'number') {
                    sortItem.attributes[sortConfig.id] = parseFloat(dataElement.textContent);
                } else {
                    sortItem.attributes[sortConfig.id] = dataElement.textContent;
                }
                sortItems.push(sortItem);
            }
        }

        return sortItems;
    }

    function sortPopulateSelectOptions(sortConfigs: SortConfig[], selectElement: HTMLSelectElement, directionElement: HTMLSelectElement): void {
        const noneOption = document.createElement('option');
        noneOption.setAttribute('value', '');
        noneOption.textContent = 'None';
        selectElement.appendChild(noneOption);

        for (const sortConfig of sortConfigs) {
            const option = document.createElement('option');
            option.setAttribute('value', sortConfig.id);
            option.textContent = sortConfig.title;
            selectElement.appendChild(option);
        }

        // Direction

        const ascOption = document.createElement('option');
        ascOption.setAttribute('value', 'asc');
        ascOption.textContent = 'Ascending';
        directionElement.appendChild(ascOption);

        const descOption = document.createElement('option');
        descOption.setAttribute('value', 'desc');
        descOption.textContent = 'Descending';
        directionElement.appendChild(descOption);
    }

    function sort(itemParent: HTMLElement, sortItems: SortItem[], sortValue: SortValue | null) {
        if (sortValue === null) {
            for (const sortItem of sortItems) {
                itemParent.appendChild(sortItem.item);
            }
        } else {
            const sortItemsCopy = sortItems.slice();

            sortItemsCopy.sort((sortItem1, sortItem2) => {
                const value1 = sortItem1.attributes[sortValue.id];
                const value2 = sortItem2.attributes[sortValue.id];

                if (value1 < value2) {
                    return sortValue.ascending ? -1 : 1;
                } else if (value1 === value2) {
                    return 0;
                } else {
                    return sortValue.ascending ? 1 : -1;
                }
            });

            for (const sortItem of sortItemsCopy) {
                itemParent.appendChild(sortItem.item);
            }
        }
    }

    function sortValueFromQuery(queryParams: { [index: string]: string }): SortValue | null {
        const sortId = queryParams['sort'];
        const sortDirection = queryParams['sort-direction'];

        if (sortId === undefined || sortId === '' || sortDirection === undefined) {
            return null;
        } else {
            return {
                id: sortId,
                ascending: sortDirection === 'asc',
            };
        }
    }

    function sortValueFromSelect(selectElement: HTMLSelectElement, directionElement: HTMLSelectElement): SortValue | null {
        const sortId = selectElement.value;
        const sortDirection = directionElement.value;

        if (sortId === '') {
            return null;
        } else {
            return {
                id: sortId,
                ascending: sortDirection === 'asc',
            };
        }
    }

    function sortValidateConfig(config: {
        itemParent: HTMLElement;
        selectElement: HTMLSelectElement;
        directionElement: HTMLSelectElement;
        fields: SortConfig[];
    }): void {
        if (!(config.itemParent instanceof HTMLElement)) {
            console.error('dwaiterCollectionSort invalid configuration option: itemParent is not an HTMLElement');
        }

        if (!(config.selectElement instanceof HTMLSelectElement)) {
            console.error('dwaiterCollectionSort invalid configuration option: selectElement is not an HTMLSelectElement');
        }

        if (!(config.directionElement instanceof HTMLSelectElement)) {
            console.error('dwaiterCollectionSort invalid configuration option: directionElement is not an HTMLSelectElement');
        }

        if (!(config.fields instanceof Array) || config.fields.length === 0) {
            console.error('dwaiterCollectionSort invalid configuration option: no fields provided');
        }

        for (const field of config.fields) {
            if (typeof field.id !== 'string') {
                console.error('dwaiterCollectionSort invalid configuration option: field id is not a string');
            }

            if (typeof field.title !== 'string') {
                console.error('dwaiterCollectionSort invalid configuration option: field (' + field.id + ') title is not a string');
            }

            if (!(field.type === 'string' || field.type === 'number')) {
                console.error("dwaiterCollectionSort invalid configuration option: field (" + field.id + ") type must be 'string' or 'number'");
            }
        }

        for (const item of Array.from(config.itemParent.children)) {
            for (const field of config.fields) {
                const elements = item.querySelectorAll(field.dataSelector);
                if (elements.length !== 1) {
                    console.error('dwaiterCollectionSort invalid configuration option: field (' + field.id + ') dataSelector matches ' + elements.length + ' elements but should match exactly 1');
                }
            }
        }

        const ids = config.fields.map(field => field.id);
        if (ids.length !== unique(ids).length) {
            console.error('dwaiterCollectionSort invalid configuration option: duplicate field id');
        }
    }


    function splitCsv(x: string): string[] {
        return x.split(',').map(y => y.trim());
    }

    function setQueryParams(params: { [index: string]: string }): void {
        let paramStrings = [];

        for (const key of Object.keys(params)) {
            paramStrings.push(key + '=' + encodeURIComponent(params[key]));
        }

        window.history.replaceState(null, '', '?' + paramStrings.join('&'));
    }

    function getQueryParams(): { [index: string]: string } {
        if (window.location.search === '') return {};

        const params: { [index: string]: string } = {};
        const items = window.location.search.substring(1).split('&');

        for (const item of items) {
            const parts = item.split('=');
            params[parts[0]] = decodeURIComponent(parts[1]);
        }

        return params;
    }

    function assertNotUndefined<T>(obj: T | undefined): T {
        if (obj === undefined) {
            throw new Error('Non-undefined assertion failed');
        } else {
            return obj;
        }
    }

    function unique(xs: string[]): string[] {
        const obj: { [index: string]: boolean } = {};

        for (const x of xs) {
            obj[x] = true;
        }

        return Object.keys(obj);
    }

    function filterInit(config: { itemParent: HTMLElement; filters: FilterConfig[] }): void {
        filterValidateConfig(config);

        const queryParams = getQueryParams();
        const items = Array.from(config.itemParent.children) as HTMLElement[];
        const filterItems = filterGetFilterItems(items, config.filters);

        for (const filterConfig of config.filters) {
            filterPopulateSelectOptions(filterConfig, filterItems);
        }

        const initialFilterValues = filterValuesFromQuery(config.filters, queryParams);

        // Set initial select value from query params
        for (const filterValue of initialFilterValues) {
            const filterConfig = assertNotUndefined(config.filters.find(filterConfig => filterConfig.id === filterValue.id));
            filterConfig.selectElement.value = filterValue.value;
        }

        // Filter initial items from query params
        if (initialFilterValues.length !== 0) {
            filter(filterItems, initialFilterValues);
        }

        // Change listeners
        for (const filterConfig of config.filters) {
            filterConfig.selectElement.addEventListener('change', onChange);
        }

        function onChange() {
            const filterValues = filterValuesFromSelects(config.filters);
            filter(filterItems, filterValues);

            const queryParams = getQueryParams();
            for (const filterConfig of config.filters) {
                const filterValue = filterValues.find(filterValue => filterValue.id === filterConfig.id);
                if (filterValue === undefined) {
                    queryParams[filterConfig.id] = '';
                } else {
                    queryParams[filterConfig.id] = filterValue.value;
                }
            }
            setQueryParams(queryParams);
        }
    }

    function sortInit(config: {
        itemParent: HTMLElement;
        selectElement: HTMLSelectElement;
        directionElement: HTMLSelectElement;
        fields: SortConfig[];
    }): void {
        sortValidateConfig(config);

        const queryParams = getQueryParams();
        const items = Array.from(config.itemParent.children) as HTMLElement[];
        const sortItems = sortGetSortItems(items, config.fields);

        sortPopulateSelectOptions(config.fields, config.selectElement, config.directionElement);

        const initialSortValue = sortValueFromQuery(queryParams);

        if (initialSortValue !== null) {
            // Set initial select value from query params
            config.selectElement.value = initialSortValue.id;
            config.directionElement.value = initialSortValue.ascending ? 'asc' : 'desc';

            // Sort initial items from query params
            sort(config.itemParent, sortItems, initialSortValue);
        }

        // Change listeners
        config.selectElement.addEventListener('change', onChange);
        config.directionElement.addEventListener('change', onChange);

        function onChange() {
            const sortValue = sortValueFromSelect(config.selectElement, config.directionElement);
            sort(config.itemParent, sortItems, sortValue);

            const queryParams = getQueryParams();
            queryParams['sort'] = config.selectElement.value;
            queryParams['sort-direction'] = config.directionElement.value;
            setQueryParams(queryParams);
        }
    }

    (window as any).dwaiterCollectionFilter = filterInit;
    (window as any).dwaiterCollectionSort = sortInit;
}());
