(function() {
    function init() {
        addSpinAnimationStyle();

        const collectionListWrappers = Array.from(document.querySelectorAll('.w-dyn-list[data-dwaiter-ajax-pagination]'));
        for (const collectionListWrapper of collectionListWrappers) {
            initCollectionListWrapper(collectionListWrapper);
        }
    }

    function initCollectionListWrapper(collectionListWrapper: Element) {
        const collectionListWrapperId = collectionListWrapper.getAttribute('data-dwaiter-ajax-pagination');
        collectionListWrapper.addEventListener('click', onClick);

        const loader = createLoader();

        function onClick(e: Event) {
            if (!(e.target instanceof Element)) return;
            const nextPreviousLink = e.target.closest('.w-pagination-next,.w-pagination-previous');
            if (!(nextPreviousLink instanceof HTMLAnchorElement)) return;

            e.preventDefault();

            showLoader(loader, nextPreviousLink);

            fetch(nextPreviousLink.href, {
                method: 'GET',
            }).then((response) => {
                return response.text();
            }).then((text) => {
                // Replace
                const domparser = new DOMParser();
                const doc = domparser.parseFromString(text, 'text/html');
                const newCollectionListWrapper = doc.querySelector('.w-dyn-list[data-dwaiter-ajax-pagination="' + collectionListWrapperId + '"]');
                if (newCollectionListWrapper === null) return;
                collectionListWrapper.innerHTML = newCollectionListWrapper.innerHTML;

                // Scroll
                const rect = collectionListWrapper.getBoundingClientRect();
                if (rect.top < 0 || rect.top > window.innerHeight) {
                    animatedScroll(rect.top + window.pageYOffset);
                }
            });
        }

        function animatedScroll(scroll: number) {
            const spring = new Spring(0.00015, 1, 0, window.pageYOffset - scroll);

            const animationLoop = new AnimationLoop((dt) => {
                // Ensure steps are small enough for a physically accurate simulation
                const maxDt = 1000 / 60;
                let remainingDt = dt;
                while (remainingDt > maxDt) {
                    spring.step(maxDt);
                    remainingDt -= maxDt;
                }
                spring.step(remainingDt);

                window.scrollTo(0, Math.round(spring.displacement + scroll));
                const stopped = Math.abs(spring.velocity) < 0.01 && Math.abs(spring.acceleration) < 0.01;
                if (stopped) {
                    animationLoop.stop();
                }
            });
        }

        function createLoader(): HTMLElement {
            const loader = document.createElement('div');
            loader.style.position = 'absolute';
            loader.style.top = '0';
            loader.style.left = '0';
            loader.style.width = '100%';
            loader.style.height = '100%';
            loader.style.display = 'flex';
            loader.style.alignItems = 'center';
            loader.style.justifyContent = 'center';

            const spinner = document.createElement('div');
            spinner.style.width = '20px';
            spinner.style.height = '20px';
            spinner.style.flexGrow = '0';
            spinner.style.flexShrink = '0';
            spinner.style.borderWidth = '2px';
            spinner.style.borderStyle = 'solid';
            spinner.style.borderLeftColor = 'transparent';
            spinner.style.borderBottomColor = 'transparent';
            spinner.style.borderRadius = '50%';
            spinner.style.animation = '1s linear infinite dwaiter-ajax-pagination-spin';

            loader.appendChild(spinner);
            return loader;
        }

        function showLoader(loader: HTMLElement, nextPreviousLink: HTMLElement) {
            const computedStyle = window.getComputedStyle(nextPreviousLink);
            if (computedStyle.position === 'static') {
                nextPreviousLink.style.position = 'relative';
            }

            for (const child of Array.from(nextPreviousLink.children)) {
                if (!(child instanceof HTMLElement || child instanceof SVGElement)) continue;
                child.style.visibility = 'hidden';
            }

            nextPreviousLink.appendChild(loader);
        }
    }

    function addSpinAnimationStyle() {
        const style = document.createElement('style');
        style.type = 'text/css';

        const keyframes = (
            '@keyframes dwaiter-ajax-pagination-spin {'
                + '0% { transform: rotate(0) }'
                + '100% { transform: rotate(360deg) }'
            + '}'
        );

        style.innerHTML = keyframes;
        document.head.appendChild(style);
    }

    class AnimationLoop {
        callback: (dt: number) => void;
        requestId: number | null;
        previousTime: number | null;
        playing: boolean;

        constructor(callback: (dt: number) => void) {
            this.callback = callback;
            this.requestId = null;
            this.previousTime = null;
            this.playing = true;

            this.step = this.step.bind(this);

            this.requestId = requestAnimationFrame(this.step);
        }

        stop() {
            if (this.requestId !== null) {
                cancelAnimationFrame(this.requestId);
            }
            this.playing = false;
        }

        step(t: number) {
            if (this.previousTime === null) {
                this.previousTime = t;

                if (this.playing) {
                    this.requestId = requestAnimationFrame(this.step);
                }
            } else {
                this.callback(t - this.previousTime);
                this.previousTime = t;

                if (this.playing) {
                    this.requestId = requestAnimationFrame(this.step);
                }
            }
        }
    }

    class Spring {
        stiffness: number;
        dampingCoefficient: number;
        velocity: number;
        displacement: number;
        acceleration: number;

        constructor(stiffness: number, dampingRatio: number, velocity: number, displacement: number) {
            this.stiffness = stiffness;
            this.dampingCoefficient = dampingRatio * 2 * Math.sqrt(stiffness);
            this.velocity = velocity;
            this.displacement = displacement;
            this.acceleration = 0;
        }

        step(dt: number) {
            // We assume a mass of 1 so force = acceleration. You can get the same effect of changing the
            // mass by changing the stiffness and dampingCoefficient
            const acceleration = -1 * this.stiffness * this.displacement - this.dampingCoefficient * this.velocity;

            this.acceleration = acceleration;
            this.velocity = this.velocity + acceleration * dt;
            this.displacement = this.displacement + this.velocity * dt;
        }
    }

    (window as any).dwaiterAjaxPagination = init;
}());
