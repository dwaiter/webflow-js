(function() {
    function init() {
        const elements = Array.from(document.querySelectorAll('[data-dwaiter-horizontal-scroll]'));
        for (const element of elements) {
            initHorizontalScroll(element);
        }
    }

    function initHorizontalScroll(element: Element) {
        const animatedScrollState = {
            playing: false,
            target: 0,
            animationLoop: null as AnimationLoop | null,
        };
        let previousVisible = true;
        let nextVisible = true;

        const slides = assertHtmlElement(element.querySelector('[data-dwaiter-horizontal-scroll-slides]'));
        const previous = assertHtmlElement(element.querySelector('[data-dwaiter-horizontal-scroll-previous]'));
        const next = assertHtmlElement(element.querySelector('[data-dwaiter-horizontal-scroll-next]'));

        previous.addEventListener('click', onPreviousClick);
        next.addEventListener('click', onNextClick);
        slides.addEventListener('scroll', onScroll, { passive: true });
        onScroll();

        function onScroll() {
            if (slides.scrollLeft < 0.5) {
                if (previousVisible) {
                    previous.style.visibility = 'hidden';
                    previousVisible = false;
                }
            } else {
                if (!previousVisible) {
                    previous.style.removeProperty('visibility');
                    previousVisible = true;
                }
            }

            const width = slides.getBoundingClientRect().width;
            const maxScroll = slides.scrollWidth - width;
            if (Math.abs(slides.scrollLeft - maxScroll) < 0.5) {
                if (nextVisible) {
                    next.style.visibility = 'hidden';
                    nextVisible = false;
                }
            } else {
                if (!nextVisible) {
                    next.style.removeProperty('visibility');
                    nextVisible = true;
                }
            }
        }

        function onPreviousClick() {
            const width = slides.getBoundingClientRect().width;
            const newDesiredScroll = (animatedScrollState.playing ? animatedScrollState.target : slides.scrollLeft) - width;
            const newScroll = Math.max(newDesiredScroll, 0);
            if (animatedScrollState.playing && newScroll === animatedScrollState.target) return;
            animatedScroll(newScroll);
        }

        function onNextClick() {
            const width = slides.getBoundingClientRect().width;
            const maxScroll = slides.scrollWidth - width;
            const newDesiredScroll = (animatedScrollState.playing ? animatedScrollState.target : slides.scrollLeft) + width;

            // Add 1px to maxScroll because for high dpi screens there is no way to calculate the fractional value,
            // so instead we overshoot by 1px which ensures we get to the end and looks fine.
            const newScroll = Math.min(newDesiredScroll, maxScroll + 1);

            if (animatedScrollState.playing && newScroll === animatedScrollState.target) return;
            animatedScroll(newScroll);
        }

        function animatedScroll(scroll: number) {
            if (animatedScrollState.playing) {
                animatedScrollStop();
            }

            animatedScrollState.playing = true;
            animatedScrollState.target = scroll;

            const spring = new Spring(0.00015, 1, 0, slides.scrollLeft - scroll);

            animatedScrollState.animationLoop = new AnimationLoop((dt) => {
                // Ensure steps are small enough for a physically accurate simulation
                const maxDt = 1000 / 60;
                let remainingDt = dt;
                while (remainingDt > maxDt) {
                    spring.step(maxDt);
                    remainingDt -= maxDt;
                }
                spring.step(remainingDt);

                if (Math.abs(spring.displacement) < 0.5) {
                    // Stop
                    slides.scrollLeft = scroll;
                    animatedScrollStop();
                } else {
                    slides.scrollLeft = spring.displacement + scroll;
                }
            });
        }

        function animatedScrollStop() {
            animatedScrollState.playing = false;

            if (animatedScrollState.animationLoop !== null) {
                animatedScrollState.animationLoop.stop();
                animatedScrollState.animationLoop = null;
            }
        }
    }

    function assertHtmlElement<T>(obj: T) {
        if (obj instanceof HTMLElement) {
            return obj;
        } else {
            throw new Error('HTMLElement assertion failed');
        }
    }

    class AnimationLoop {
        callback: (dt: number) => void;
        requestId: number | null;
        previousTime: number | null;
        playing: boolean;

        constructor(callback: (dt: number) => void) {
            this.callback = callback;
            this.requestId = null;
            this.previousTime = null;
            this.playing = true;

            this.step = this.step.bind(this);

            this.requestId = requestAnimationFrame(this.step);
        }

        stop() {
            if (this.requestId !== null) {
                cancelAnimationFrame(this.requestId);
            }
            this.playing = false;
        }

        step(t: number) {
            if (this.previousTime === null) {
                this.previousTime = t;

                if (this.playing) {
                    this.requestId = requestAnimationFrame(this.step);
                }
            } else {
                this.callback(t - this.previousTime);
                this.previousTime = t;

                if (this.playing) {
                    this.requestId = requestAnimationFrame(this.step);
                }
            }
        }
    }

    class Spring {
        stiffness: number;
        dampingCoefficient: number;
        velocity: number;
        displacement: number;
        acceleration: number;

        constructor(stiffness: number, dampingRatio: number, velocity: number, displacement: number) {
            this.stiffness = stiffness;
            this.dampingCoefficient = dampingRatio * 2 * Math.sqrt(stiffness);
            this.velocity = velocity;
            this.displacement = displacement;
            this.acceleration = 0;
        }

        step(dt: number) {
            // We assume a mass of 1 so force = acceleration. You can get the same effect of changing the
            // mass by changing the stiffness and dampingCoefficient
            const acceleration = -1 * this.stiffness * this.displacement - this.dampingCoefficient * this.velocity;

            this.acceleration = acceleration;
            this.velocity = this.velocity + acceleration * dt;
            this.displacement = this.displacement + this.velocity * dt;
        }
    }

    (window as any).dwaiterHorizontalScroll = init;
}());
